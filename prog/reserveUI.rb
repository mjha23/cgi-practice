#!/usr/bin/ruby  
require 'cgi'
require_relative 'Transaction'
require_relative 'Patron'
require_relative 'Book'

query = CGI.new("html5")

transaction = Transaction.new
returnResults = transaction.reserveBook(query['isbn'], query['memberID'])
confirmation = ""

if (returnResults == "Invalid ISBN")
        confirmation = "Error processing transaction: Please check the ISBN and try again"
elsif (returnResults == "Invalid memberID")
	confirmation = "Error processing transaction: Please check the memberID and try again"
elsif (returnResults == "Already Reserved")
        confirmation = "Error processing transaction: Book is already On Hold"
else
        confirmation = "Book Reservation Successful!"
end

query.out do
query.html do
query.head { query.title { "SmartLibrary: Reserve Book" } } +
query.body do
query.form do
        query.h1{ "Place Book On Hold" } +
        query.hr +
        query.h3{"#{confirmation}"} +
        query.br +
        query.a("http://www.mayanksprojects.com/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end



