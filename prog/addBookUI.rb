#!/usr/bin/ruby  
require 'cgi'
require_relative 'Book'
require 'mysql2'

query = CGI.new("html5")

addbook = Book.new
confirmationMsg = addbook.addToInventory(query['isbn'], query['title'], query['author'], query['location'])

query.out do                
query.head { query.title { "SmartLibrary: Search" } } +                         
query.form do
	query.h1{ "Add Book to Inventory" } +
	query.hr +
        query.h3{"You have successfully added #{confirmationMsg} to the inventory!"} +
	query.br +
	query.a("http://www.mayanksprojects.com/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end

